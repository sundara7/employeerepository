package com.api.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.api.BaseTest;
import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.service.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = EmployeeController.class)
public class EmployeeControllerTest extends BaseTest {

	@InjectMocks
	private EmployeeController controller;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private EmployeeService service;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void createEmployeeDataWhenCreateEmployeeIsCalled() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(service.create(Mockito.any(Employee.class))).thenReturn(employee);
		String employeeRequestJson = "{\"id\":1,\"name\":\"Employee1\",\"address\":\"22 Fairylane Circle, Dearborn, Michigan\",\"dateOfBirth\":\"07-07-1990\",\"departments\":[{\"id\":1,\"name\":\"department1\"}]}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/employee").accept(MediaType.APPLICATION_JSON)
				.content(employeeRequestJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());
		JSONAssert.assertEquals(employeeRequestJson, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void retrieveEmployeeDataBasedOnEmployeeId() throws Exception {
		Mockito.when(service.fetchById(1)).thenReturn(getEmployee());
		String employeeResponseJson = "{\"id\":1,\"name\":\"Employee1\",\"address\":\"22 Fairylane Circle, Dearborn, Michigan\",\"dateOfBirth\":\"07-07-1990\",\"departments\":[{\"id\":1,\"name\":\"department1\"}]}";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/employee/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());
		JSONAssert.assertEquals(employeeResponseJson, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void returnHttpStatusNotFoundWhenEmployeeRecordNotFound() throws Exception {
		Mockito.doThrow(RecordNotFoundException.class).when(service).fetchById(1);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/employee/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(404, result.getResponse().getStatus());
	}
	
	@Test
	public void returnBadRequestWhileCreatingEmployeeDataWhenEmployeeIdIsNull() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(service.create(Mockito.any(Employee.class))).thenReturn(employee);
		String employeeRequestJson = "{\"name\":\"Employee1\",\"address\":\"22 Fairylane Circle, Dearborn, Michigan\",\"dateOfBirth\":\"07-07-1990\",\"departments\":[{\"id\":1,\"name\":\"department1\"}]}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/employee").accept(MediaType.APPLICATION_JSON)
				.content(employeeRequestJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(400, result.getResponse().getStatus());
	}
	
	@Test
	public void returnBadRequestWhileCreatingEmployeeDataWhenEmployeeNameIsEmpty() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(service.create(Mockito.any(Employee.class))).thenReturn(employee);
		String employeeRequestJson = "{\"id\":1,\"name\":\"\",\"address\":\"22 Fairylane Circle, Dearborn, Michigan\",\"dateOfBirth\":\"07-07-1990\",\"departments\":[{\"id\":1,\"name\":\"department1\"}]}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/employee").accept(MediaType.APPLICATION_JSON)
				.content(employeeRequestJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(400, result.getResponse().getStatus());
	}
	
	@Test
	public void returnBadRequestWhileCreatingEmployeeDataWhenDepartmentNameIsNull() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(service.create(Mockito.any(Employee.class))).thenReturn(employee);
		String employeeRequestJson = "{\"id\":1,\"name\":\"Employee1\",\"address\":\"22 Fairylane Circle, Dearborn, Michigan\",\"dateOfBirth\":\"07-07-1990\",\"departments\":[{\"id\":1,\"name\":null}]}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/employee").accept(MediaType.APPLICATION_JSON)
				.content(employeeRequestJson).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		assertEquals(400, result.getResponse().getStatus());
	}

}
