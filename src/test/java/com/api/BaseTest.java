package com.api;

import java.util.ArrayList;
import java.util.List;

import com.api.domain.Department;
import com.api.domain.Employee;

public class BaseTest {

	public Employee getEmployee() {
		Employee employee = new Employee();
		employee.setId(1);
		employee.setName("Employee1");
		employee.setDateOfBirth("07-07-1990");
		employee.setAddress("22 Fairylane Circle, Dearborn, Michigan");

		List<Department> departments = new ArrayList<Department>();
		Department department = new Department();
		department.setId(1);
		department.setName("department1");
		departments.add(department);
		employee.setDepartments(departments);
		return employee;
	}
}
