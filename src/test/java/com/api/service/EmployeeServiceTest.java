package com.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.api.BaseTest;
import com.api.domain.Department;
import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.repository.EmployeeRepository;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class EmployeeServiceTest extends BaseTest {

	@InjectMocks
	private EmployeeService service;
	
	@Mock
	private EmployeeRepository repository;

	@Test
	public void createEmployeeRecord() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(repository.save(employee)).thenReturn(employee);

		Employee createdEmployee = service.create(employee);

		assertEmployeeData(employee, createdEmployee);
	}

	@Test
	public void retrieveEmployeeRecord() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(repository.findById(1)).thenReturn(Optional.of(employee));
		
		Employee retrievedEmployee = service.fetchById(1);
		
		assertEmployeeData(employee, retrievedEmployee);
	}
	
	@Test
	public void createEmployeeRecordWhenDepartmentDoesNotExists() throws Exception {
		Employee employee = getEmployee();
		List<Department> departments = new ArrayList<Department>();
		Department department = new Department();
		department.setName("department2");
		departments.add(department);
		employee.setDepartments(departments);
		Mockito.when(repository.save(employee)).thenReturn(employee);

		Employee createdEmployee = service.create(employee);

		assertEmployeeData(employee, createdEmployee);
	}
	
	@Test
	public void throwsRecordNotFoundExceptionWhenEmployeeNotFoundById()
			throws Exception {
		RecordNotFoundException exception = Assertions.assertThrows(
				RecordNotFoundException.class, () -> {
					service.fetchById(1);
				});
		
		Assertions.assertEquals("Employee record not found",
				exception.getMessage());
	}
	
	private void assertEmployeeData(Employee employee, Employee createdEmployee) {
		Assertions.assertEquals(employee.getId(), createdEmployee.getId());
		Assertions.assertEquals(employee.getName(), createdEmployee.getName());
		Assertions.assertEquals(employee.getDateOfBirth(),
				createdEmployee.getDateOfBirth());
		Assertions.assertEquals(employee.getAddress(),
				createdEmployee.getAddress());
		Assertions.assertEquals(employee.getDepartments().size(),
				createdEmployee.getDepartments().size());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getId(), createdEmployee.getDepartments().stream()
				.findFirst().get().getId());
		Assertions.assertEquals(employee.getDepartments().stream().findFirst()
				.get().getName(), createdEmployee.getDepartments().stream()
				.findFirst().get().getName());
	}

}
