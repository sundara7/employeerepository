package com.api.exception;

public class RecordNotFoundException extends Exception {

	private static final long serialVersionUID = -370713365060456323L;
	
	public RecordNotFoundException(String errorMessage) {
		super(errorMessage);
	}

}
