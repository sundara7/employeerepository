package com.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository repository;
	
	public Employee create(Employee employee) {
		employee.getDepartments().forEach(department -> department.setEmployee(employee));
		return repository.save(employee);
	}

	public Employee fetchById(Integer id) throws RecordNotFoundException {
		Optional<Employee> employee = repository.findById(id);
		if (!employee.isPresent()) {
			throw new RecordNotFoundException("Employee record not found");
		}
		return employee.get();
	}

}
